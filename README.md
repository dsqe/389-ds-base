389 Directory Server
====================

**ATTENTION: This repository has been moved to GitHub: [389ds/389-ds-base](https://github.com/389ds/389-ds-base)**

**Please open any issue or pull request there.**

This repository on Pagure is still available to not break any existing links. However,
we do not pay any attention here anymore.

## Quick links to GitHub repository:

* **Code**: https://github.com/389ds/389-ds-base
* **Issue Tracker**: https://github.com/389ds/389-ds-base/issues
* **Pull Requests**: https://github.com/389ds/389-ds-base/pulls
* **Documentation**: https://www.port389.org/
* **Releases**: https://github.com/389ds/389-ds-base/releases

